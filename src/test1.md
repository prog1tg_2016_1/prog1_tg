[http://www.markmyprofessor.com/](http://www.markmyprofessor.com/)

A TTIK ?ltal az ETR-ben 2012-2013-1-t?l v?gzett oktat?si munka hallgat?i v?lem?nyez?se

J? fej tan?r! Jah NEM! A gyakorlat?n, ha az emberek 10% ?tmegy az m?r sok! (...) Na meg, hogy itt milyen v?lem?nnyel vannak r?la. Tan?r ?r aj?nlan?m figyelm?be, hogy tal?n, de csak TAL?N lehet, mag?ban van a hiba! (...)

Szerintem nem val?sz?n?, hogy ?l ember a f?ld?n, aki ezt a t?rgyat n?la jobban el? tudja adni - az anyagot igyekszik a lehet? legjobban, ?letszer? p?ld?kkal leadni, n?ha humorral fel?breszteni a t?rsas?got.

---------- O ------------

A t?rgy maga nem teljes?thetetlen, s?t! T?nyleg rengeteg az anyag, viszont a hozz??ll?s?val m?r bajok ad?dtak. Nem tudta megem?szteni az itteni negat?v kritik?kat, mindig azokkal hozakodott el?.

Egy olyan oktat?, akit t?nyleg ?rdekel, hogy di?kjai hogyan teljes?tenek, ami manaps?g el?gg? p?lda?rt?k?. Nem csak az?rt j?r be, hogy ledar?lja az ?ra anyag?t, hanem hogy meg?rtesse m?sokkal. Az ?r?i humorosak, amivel pr?b?lja oldani a csendet a teremben.:D

---------- O ------------

A r?pikben olyan feladatokat kaptunk, hogy csak n?ztem.Nem mondan?m, hogy teljesen az el?z? ?rai anyagot t?kr?zte.

Kis ZH-n?l korrekt, mindig azt k?ri vissza, amit leadott el?z? ?r?n ?s m?g az nap ki is jav?tja. Egyik legjobb tan?r az egyetemen.:)

---------- O ------------

M?g egy ilyen fellengz?, tud?s?val k?rked?, sunyi tan?rt nem hordott h?t?n a f?ld. Az els? k?t h?t ut?n garant?lt volt a buk?s eme t?rgyb?l. El?felt?tel?t els?re, j?nak mondhat? eredm?nnyel teljes?tettem, de ami itt volt, az abszurd. A Tan?r ?r nem tudott elvonatkoztatni a szem?lyes negat?v kritik?kt?l, amiket el?z? ?vekben kapott. A st?lusa sz?rny?, ak?rmi baj volt, mindig azzal hozakodott el?, hogy lehet menni a d?nt?shoz?khoz panaszra, akik ennyi anyagot zs?foltak ?ssze. Volt ?ra, mikor egy hallgat?t kisz?l?tott a t?bl?hoz, ?s 1,5 ?ra alatt se tudta az ?j anyagot megoldani, erre annyit reag?lt, hogy a t?rsuknak k?sz?nhet?en tanulj?k meg otthon. Az anyag nem volna v?szes, a t?rgy is teljes?thet? lenne, ha az lenen sz?mon k?rve, amit lead. ?r?n gyakoroltuk az A ?s B t?pus? feladatokat, ? meg sz?mon k?rte a C-t.

A legjobb tan?rom mi?ta az egyetemre j?rok. Ez volt a kedvenc el?ad?som! Az ilyen tan?rok miatt lehet?nk b?szk?k hogy SZTEsek vagyunk.

Komment t?nyleg szar. Egy?bk?nt meg ha tanulsz eleget(t?nyleg iszonyat sokat), akkor se a kalkulus se az an?l nem teljes?thetetlen ?s csak egy tan?rt kapsz, aki pont olyan,mint Dr. House. Nyers ?s vicces. Mellesleg iszonyat j?l ?tadja az anyagot is,csak senki se ?gy tanul,mint ahogy ? azt k?ri,ill. tan?tja. Semminek nem ?r?ltem annyita az egyetemen,mint az anal?zis 3asnak, ?s m?r sajn?lom,hogy nem tart t?bb ?r?t nekem:(

---------- O ------------

Ink?bb hagyjuk. . . NO COMMENT! :-( :-@ ?vek ?ta gondolom csak a leh?z?s megy, de semmi ?rtelme. Semmi se v?ltozott. Az eg?sz ?gy rossz ahogy van s ezt ?gy mondom, hogy els?re vettem fel a t?rgyat els?re siker?lt is. Eddig csak ?rdekess?gk?pp j?rtam be felk?sz?tve ?nnmagamat arra ami ott fog v?rni. Sz?gyen ?s gyal?zat ami a kurzuson folyik. :-(

?lm?ny volt minden ?ra, a Tan?r?rn?l jobban nem hiszem, hogy b?rki el? tudn? adni ezt az anyagot. Nem a matek a kedvenc tant?rgyam, de ?lvezettel j?rtam be az ?r?kra, ami soha nem volt unalmas, mert igyekezett humorral feldobni ezt az egy?ltal?n nem k?nny? t?rgyat. M?g sok sok ilyen oktat? kellene :)

?

Nagyon sokszor ?reztette vel?nk az Oktat?, hogy mennyire but?k, alul m?veltek vagyunk ?s hogy mennyire len?zi az informatikusokat. Az els? el?ad?st?l kezdve ?gy ?reztem, arra j?tszik, hogy min?l kevesebben, vagy ink?bb senki se j?rjon be el?ad?sra, hogy neki ne legyen dolga.

Nagyon szerettem az el?ad?sra bej?rni, az anyag vil?gos, ?rthet?, a hangulat kiv?l? volt. Az egyik legjobb el?ad?s volt minden eddig l?tott el?ad?saim k?z?l. Az el?ad? kiv?l?an magyar?zott, az anyagot a leg?rdekesebben, legsz?nesebben t?lalta. Egyszer?en nem tudtam nem odafigyelni.

---------- O ------------

?r?ltem, hogy egyik ?r?n felhozta, hogy nyugd?jba megy j?v?re. M?r alig v?rom. Olvastam, hogy kapott Aranykr?ta D?jat. T?nyleg ? lenne a k?vetend? p?lda? Id?ig jutottunk. Ha j?v?re marad, n?la veszem fel a gyakorlatot is, mert ez nem ?llapot hogy sehogy nem lehet ?tmenni. 100ezres nagys?grendbe k?lt?ttem min?s?gi mag?ntan?rokra,egyetemi matematika szakon tan?t? professzorokra, hogy felk?sz?tsenek tan?v alatt ?s az elm?lt 2 ny?ri sz?netben is. Legal?bb 1000 ?r?t r?sz?ntam m?r csak erre a t?rgyra. M?g mindig magamba keressem a hib?t ?s magamat okoljam a kudarcok?rt ?s a f?lresiklott j?v?m miatt?

A hallgat?k t?bbs?ge nem a kurzuson tanultak miatt halad nehezen, hanem a sz?ks?ges el?ismeretek hi?nyoznak. Sajnos ezt csak a Kalkulus I.-en tanultak m?lyebb elsaj?t?t?s?val lehetne megoldani. Az el?ad?sok nagyon ?rthet?ek, az el?ad?sm?d pedig szenz?ci?s! Ilyen tan?rb?l kellene m?g.

---------- O ------------

El?ad?sokon semmit nem magyar?z, azt v?rja, hogy majd a gyakorlatvezet?k tan?ts?k meg, amit tudnunk k?ne. A di?kokkal egy?ltal?n nem foglalkozik, csak az a fontos sz?m?ra, hogy a minimumot ?tengedje. Nem engedn?m tan?tani az ilyet.

Mind az el?ad?son, mind a gyakorlaton t?rekszik az ?rthet? magyar?zatra, szeml?letes p?ld?kkal teszi szinesebb? az ?r?kat. Az ?r?it l?togatni m?r a saj?tos st?lusa miatt is meg?rte, felvillanyozta az eg?sz napot az ?rad? j? hangulat?val. A gyakorlatokon a di?kok magukat tehetik pr?b?ra az ?j kih?v?sokkal, ha valamit nem ?rtenek, a k?rd?seikre v?laszt kapnak. Nincs rohan?s a gyakorlatokon, lehet hogy csak n?h?ny p?lda bemutat?s?ra jut id?, de azt teljesen ?tbesz?lik. Aki a quizekb?l ?ssze tudja szedni a 10 pontot annak a vizsg?n sem lehet nagy probl?m?ja. ?gy nem tanultunk semmit, hogy k?zben egyre komplexebb feladatokat tudtunk megoldani :)

?

---------- O ------------

Borzalmas egy t?rgy...

El?gedett vagyok a t?rggyal.