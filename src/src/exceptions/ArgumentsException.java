package exceptions;

public class ArgumentsException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1656228607077089295L;

	public ArgumentsException(String s) {
		super(s);
	}
	
	public ArgumentsException() {
		
	}
	
}