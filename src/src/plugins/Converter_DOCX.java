package plugins;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.docx4j.Docx4J;
import org.docx4j.Docx4jProperties;
import org.docx4j.convert.out.html.AbstractHtmlExporter.HtmlSettings;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.overzealous.remark.Remark;

import exceptions.InvalidFormatException;

/** DOCX �talak�t�
 * @author Bencsik D�vid */
public class Converter_DOCX extends Converter{
	
	public final String format = "docx";
	public String author = "Bencsik D�vid";
	public String version = "V0.1";
	
	@Override
	public int convert(String input, String output) throws InvalidFormatException {
		File inFile = new File(input);
		File outFile = new File(output);
		
		if(inFile.isFile()) {
			PrintStream err = System.err;
			System.setErr(new PrintStream(new OutputStream() {
			    @Override public void write(int b) throws IOException {}
			}));
			try {
				Docx4jProperties.getProperties().setProperty(
					    "docx4j.Log4j.Configurator.disabled", "true");
				//PropertyConfigurator.configure("");
				Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML", true);		    					
				
				WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(inFile);
			
				// XHTML export		
		    	@SuppressWarnings("deprecation")
				HtmlSettings htmlSettings = new HtmlSettings();
		    	
		    	htmlSettings.setWmlPackage(wordMLPackage);
		    	
		    	htmlSettings.setImageDirPath(inFile.getPath() + "_files");
		    	htmlSettings.setImageTargetUri(inFile.getPath() + "_files");
		    	
		    	String htmlFilePath = "temp.html";
		    	OutputStream os = new java.io.FileOutputStream(htmlFilePath, false);		
								
				Docx4J.toHTML(htmlSettings, os, Docx4J.FLAG_NONE);				
				os.flush();
				os.close();				
				
				//Remark remark = new Remark();
				//String markdown = remark.convert(new File("temp.html"));
				//String markdown = "";
				Converter_HTML htmlconv = new Converter_HTML();
				htmlconv.convert("temp.html", output);
				//System.out.println(markdown);
				File f = new File(htmlFilePath);
				f.delete();

				//BufferedWriter writer = new BufferedWriter( new FileWriter(outFile));
				//writer.write(markdown);
				
				//writer.flush();
				//writer.close();
			} catch(IOException e){
				System.setErr(err);
				
				System.err.println("Nem siker�lt a f�jlm�velet :(");
			} catch (Docx4JException e) {
				throw new InvalidFormatException();				
			} finally{
				System.setErr(err);				
			}
		}
		
		
		return 0;
	}
	
}
