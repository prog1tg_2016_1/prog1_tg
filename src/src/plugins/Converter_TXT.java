package plugins;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import exceptions.*;

public class Converter_TXT extends Converter {
	public static final String format = "txt";
	public static String author = "Gera Imre";
	public static String version = "V0.1";
	
	public int convert(String input, String output) throws InvalidFormatException {

		try {
			String contents = "";
			String thisline = "";
			
			BufferedReader freader = new BufferedReader(new FileReader(input));
			while((thisline = freader.readLine()) != null) {
				contents += thisline + "\n";
			}
			freader.close();
			
			FileWriter fwriter = new FileWriter(output);
			fwriter.write(contents);
			fwriter.close();
		}
		catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}
	
		
		
		
		return 0;
	};
	
	public String toString() {
		return "TXT Converter (not working) by Imre Gera.";
	}
}
