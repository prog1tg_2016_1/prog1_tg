package plugins;

import java.io.File;
import java.io.FileWriter;
import java.util.Iterator;

import org.odftoolkit.simple.*;
import org.odftoolkit.simple.text.Paragraph;

import exceptions.InvalidFormatException;

public class Converter_ODF extends Converter {
	public String format = "odt";
	public String author = "Gera Imre / GEIXAAT.SZE";		// shall be final in implementation
	public String version = "v0.1";		// shall be final in implementation
	
	private String mdContent = "";
	
	public int convert(String input, String output) throws InvalidFormatException {
		
		
		TextDocument odfdoc;
		
		try {
			odfdoc = TextDocument.loadDocument(new File(input));
			int linecount = 1;
			
			Iterator<Paragraph> odfit =	odfdoc.getParagraphIterator();
			
			while(odfit.hasNext()) {
				try {
					if(linecount == 1) {
						mdContent += "**";
						mdContent += odfit.next().getTextContent() + "**\n";
					}
					else {
						mdContent += odfit.next().getTextContent() + "\n";
					}
					
					linecount++;
				}
				catch(Exception e) {
					System.out.println("Hiba: az ODF konvert�l� hib�ba �tk�z�tt!");
				}
			}
			
			FileWriter outwriter = new FileWriter(output);
			outwriter.write(mdContent);
			outwriter.close();
			
			
			//writeMD(output);		// wtf, this is not working
		} catch (Exception e) {
			// main.Main.writeLog("IO Hiba!");
			System.out.println("IO Hiba");
			e.printStackTrace();
		}
		
		return 0;
	}
}
