package plugins;

import exceptions.InvalidFormatException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/** Konverter interf�sz
 * @author Gera Imre / GEIXAAT.SZE */
public abstract class Converter {

	public String format = null;
	public String author = null;		// shall be final in implementation
	public String version = null;		// shall be final in implementation
	
	private String mdContent = "";

	
	public abstract int convert(String input, String output) throws InvalidFormatException;
	
	public int writeMD(String outputFileName) {
		
		BufferedWriter outWriter;
		try {
			outWriter = new BufferedWriter(new FileWriter(outputFileName));
			outWriter.write(this.mdContent);
			outWriter.close();
		}
		catch(IOException e) {
			//main.Main.writeLog("IO Kiv�tel a(z) " + outputFileName + " f�jl ki�r�sakor!");
			System.out.println("IO Kiv�tel a(z) " + outputFileName + " f�jl ki�r�sakor!");
			//main.Main.writeLog("IO Kiv�tel a(z) " + outputFileName + " f�jl ki�r�sakor!");
			return 1;
		}
		
		//File output = new File(outputFileName);
				
		
		
		
		return 0;
	}
}
