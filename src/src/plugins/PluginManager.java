package plugins;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/** Plug-in handler main class. Loading happens from "plugins.txt"
 * @author Gera Imre / GEIXAAT.SZE
 * @version V0.2.1*/
public class PluginManager {
	
	private static Converter[] pluginList;
	private String[] extensions;
	
	public Converter[] getAvailablePlugins() {
		ArrayList<Converter> converters = new ArrayList<Converter>();
		ArrayList<String> extensionsList = new ArrayList<String>();
		BufferedReader reader;
		
		try {
			reader = new BufferedReader(new FileReader("plugins.txt"));
			
			String currentLine;
			while((currentLine = reader.readLine()) != null) {
			
				Class<?> currentPlugin;
				
				try {
					currentPlugin = Class.forName("plugins." + currentLine);
					boolean isConverter = false;
					
					/*for(Class<?> i : currentPlugin.getInterfaces()) {
						if(Converter.class.equals(i)) {
							isConverter = true;
							break;
						}
					}*/
					if(Converter.class.equals(currentPlugin.getSuperclass()))
						isConverter = true;
					
					if(!isConverter) {
						// print error that this is not a plug-in :/
						System.out.println("-- Hib�s be�p�l�: " + currentPlugin.getName());						
					}
					else {
						Converter currentInstance;
						try {
							currentInstance = (Converter)currentPlugin.newInstance();
							converters.add(currentInstance);
							
							String format = "<none>";
							try {
								format = (String)currentPlugin.getField("format").get(currentInstance);
								
								if(format == null || format == "<none>") {
									System.out.println("-- A " + currentPlugin.getName() + " plugin form�tuma nem ismerhet� fel!");
									continue;
								}
								
								extensionsList.add(format);
								
							} catch (Exception e) {
								System.out.println("-- A " + currentPlugin.getName() + " plugin form�tuma nem ismerhet� fel!");
							}
							
							System.out.println("++ Plugin bet�ltve: " + currentPlugin.getName() + " | t�mogatott form�tum: " + format);
						} catch (InstantiationException e1) {
							System.out.println("-- Hiba: InstantiationException");
						} catch (IllegalAccessException e1) {
							System.out.println("-- Hiba: hozz�f�r�s megtagadva!");
						}
					}
					
				} catch (ClassNotFoundException e) {
					// print error line to Main stating a requested plug-in is not found
					System.out.println("-- Oszt�ly nem t�lthet� be | " + e.getMessage());
				}
			}
			
			reader.close();
			//Class[] returnArray = new Class[converters.size()];
			pluginList = new Converter[converters.size()];
			extensions = new String[extensionsList.size()];
			
			//converters.toArray(pluginList);
			extensionsList.toArray(extensions);
			
			return converters.toArray(pluginList);
		
		}
		catch (IOException e) {
			System.out.println("A plugins.txt nem t�lthet� be!");
			return null;
		}
	
	}
	
	public static Converter getConverter(String filename) {
		StringTokenizer splitter = new StringTokenizer(filename, ".");
		String lastPart = "";
		while(splitter.hasMoreTokens()) {
			lastPart = splitter.nextToken();
		}
		
		
		for(Converter item : pluginList) {
			
			if(getAttribute(item, "format") == null)
				continue;
			else
				//System.out.println("F�jlt�pus: " + lastPart + ", jelenlegi: " + getAttribute(item, "format"));
	
			
			if(lastPart.toLowerCase().equals(getAttribute(item, "format").toLowerCase())) {
				return item;
			}
		}
		
		return null;
	}
	
	public static String getAttribute(Converter converter, String attribute) {
		try {
			return (String) converter.getClass().getField(attribute).get(converter);
		} catch (Exception e) {
			return null;
		}
	}
}
