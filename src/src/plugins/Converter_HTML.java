package plugins;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import exceptions.InvalidFormatException;

public class Converter_HTML extends Converter {
	public final String format = "html";
	public String author = "Kovacs Marcell";
	public String version = "V0";

	public int convert(String input, String output) throws InvalidFormatException {
	
		try {
	         String pathFile = input;
	         File f = new File(pathFile);
	         String parsedFileText = HTML2MD.HTML2Md.convertFile(f, "gbk");
	         //System.out.println(parsedFileText);
	         FileWriter outWriter = new FileWriter(output);
	         outWriter.write(parsedFileText);
	         outWriter.close();
	     } catch (IOException e) {
	         System.out.println("IO Hiba: a(z) " + input + " -> " + output + " konvertálás nem lehetséges!");
	     }
		
		return 0;
	}
}
