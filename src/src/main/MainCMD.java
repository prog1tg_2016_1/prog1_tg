package main;

import java.util.ArrayList;

import plugins.*;

import exceptions.ArgumentsException;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
	Main osztaly a konvertalo program parancssori vezerlesehez.
	Feldolgozza a parancssori argumentumokat, ezek alapjan kezeli a pluginlistat, vezerli a konvertalo osztalyokat.
	@author Kocsar Mate, KOMXAAT.SZE
*/
public class MainCMD {
	
	public static void main(String[] args) {
		
		System.out.println("MD Konvertáló, írta: Bencsik Dávid, Gera Imre, Kocsár Máté, Kovács Marcell\n");
		
		// pluginlista ellenorzese, ha kell letrehozasa, azonos sorok szurese
		try {
			File plugins = new File("plugins.txt");
			if (!plugins.exists()) plugins.createNewFile();
			else {
				ArrayList<String>  pluginsTemp = new ArrayList<String>();
				BufferedReader reader = new BufferedReader(new FileReader("plugins.txt"));
				String line;
				while ((line = reader.readLine()) != null ) {
					if (!pluginsTemp.contains(line)) pluginsTemp.add(line);
				}
				reader.close();
				
				FileWriter writer = new FileWriter(new File("plugins.txt"));
				for (int i=0; i<pluginsTemp.size(); ++i) {
					writer.write(pluginsTemp.get(i));
					writer.write(System.lineSeparator());
				}
				writer.close();
				
				PluginManager manager = new PluginManager();
				manager.getAvailablePlugins(); // initialize everything
			}
		} catch (Exception e) {
			System.out.println("Hiba lepett fel a pluginek betoltesekor.");
			System.exit(1);
		}
		
		try {
			
			// nincs argumentum
			if (args.length<1) throw new ArgumentsException("a program argumentumokat var");
			
			// segitseg
			else if (args[0].equals("help") || args[0].equals("info")) {
				try {
					BufferedReader reader = new BufferedReader(new FileReader("help.txt"));
					String line;
					while ((line = reader.readLine()) != null ) {
						System.out.println(line);
					}
					reader.close();
				} catch (Exception e) {
					System.out.println("lasd: help.txt");
				}
			}
			
			// pluginek kiirasa
			else if(args[0].equals("-lp") || args[0].equals("--listplugins")) {
				if (args.length==1) {
					try {
						BufferedReader reader = new BufferedReader(new FileReader("plugins.txt"));
						String line;
						while ((line = reader.readLine()) != null ) System.out.println(line);
						reader.close();
					} catch (IOException e) {
						System.out.println("Hiba lepett fel a pluginek betoltesekor.");
						System.exit(1);
					}
				} else throw new ArgumentsException();
			}
			
			// plugin hozzaadasa
			else if(args[0].equals("-ap") || args[0].equals("--addplugin")) {
				if (args.length==1) throw new ArgumentsException("hianyzo argumentum: hozzaadando plugin");
				else try {
					ArrayList<String> pluginsTemp = new ArrayList<String>();
					BufferedReader reader = new BufferedReader(new FileReader("plugins.txt"));
					String line;
					while ((line = reader.readLine()) != null ) {
						pluginsTemp.add(line);
					}
					reader.close();
					
					for (int i=1; i<args.length; ++i) {
						if (pluginsTemp.contains(args[i])) {
							System.out.println("A pluginlista mar tartalmazza: " + args[i]);
						} else pluginsTemp.add(args[i]);
					}
					
					FileWriter writer = new FileWriter(new File("plugins.txt"));
					for (int i=0; i<pluginsTemp.size(); ++i) {
						writer.write(pluginsTemp.get(i));
						writer.write(System.lineSeparator());
					}
					writer.close();
				} catch (IOException e){
					System.out.println("Hiba lepett fel a pluginlista modositasakor.");
				}
			}
			
			// pluginek eltavolitasa
			else if(args[0].equals("-rp") || args[0].equals("--removeplugin")) {
				if (args.length==1) throw new ArgumentsException("hianyzo argumentum: eltavolitando plugin");
				else try {
					ArrayList<String> pluginsTemp = new ArrayList<String>();
					BufferedReader reader = new BufferedReader(new FileReader("plugins.txt"));
					String line;
					while ((line = reader.readLine()) != null ) {
						pluginsTemp.add(line);
					}
					reader.close();
					
					for (int i=1; i<args.length; ++i) {
						if (pluginsTemp.contains(args[i])) pluginsTemp.remove(args[i]);
						else {
							System.out.println("A pluginlista nem tartalmazza: " + args[i]);
						}
					}
					
					FileWriter writer = new FileWriter(new File("plugins.txt"));
					for (int i=0; i<pluginsTemp.size(); ++i) {
						writer.write(pluginsTemp.get(i));
						writer.write(System.lineSeparator());
					}
					writer.close();
				} catch (IOException e) {
					System.out.println("Hiba lepett fel a pluginlista modositasakor.");
				}
			}
			
			// konvertalas
			else {
				/*
					itt kellene majd betolteni kb a PluginManagert
				*/
				
				boolean dirs = false;
				boolean force = false;
				ArrayList<String> inputFiles = new ArrayList<String>();
				ArrayList<String> outputFiles = new ArrayList<String>();
				
				// argumentumok feldolgozasa, eltarolasa
				for (int i=0; i<args.length; ++i) {
					if (args[i].equals("-d") || args[i].equals("--directory")) dirs = true;
					else if (args[i].equals("-f") || args[i].equals("--force")) force = true;
					else {
						if ("md".equals(getFileFormat(args[i])) && !args[i].equals(".md")) {
							if (!outputFiles.contains(args[i])) outputFiles.add(args[i]);
							else throw new ArgumentsException("ismetlodo output file");
						} else {
							if (!inputFiles.contains(args[i])) inputFiles.add(args[i]);
							else throw new ArgumentsException("ismetlodo input file, vagy mappa");
						}
					}
				}
				
				// mappa(k) konertalasa
				if (dirs) {
					if (outputFiles.size()!=0) throw new ArgumentsException("mappa konvertalasakor nem lehet megadni output file-t");
					
					for (int i=0; i<inputFiles.size(); ++i) {
						try {
							File dir = new File(inputFiles.get(i));
							if (!dir.exists()) {
								System.out.println("Nem letezo mappa: " + inputFiles.get(i));
								
							} else if (!dir.isDirectory()) {
								System.out.println("Nem mappa: " + inputFiles.get(i));
								
							} else {
								File[] fileok = dir.listFiles();
								for (int j=0; j<fileok.length; ++j) {
									String inFileName = dir.getName() + "/" + fileok[j].getName();
									
									File inFile = new File(inFileName);
									if (inFile.isFile() /* && PluginManager.getConverter(inFileName) != null */) {
										giveToConverter(inFileName, getFileName(inFileName) + ".md", force);
									}
								}
							}
							
						} catch (Exception e){
							System.out.println("Hiba tortent a mappa konvertalasa soran: " + inputFiles.get(i));
						}
					}
				
				// fajlok konvertalasa
				} else {
					if (outputFiles.size()==0) {
						for (int i=0; i<inputFiles.size(); ++i) {
							giveToConverter(inputFiles.get(i), getFileName(inputFiles.get(i)) + ".md", force);
						}
						
					} else if (outputFiles.size()==1) {
						for (int i=0; i<inputFiles.size(); ++i) {
							giveToConverter(inputFiles.get(i), getFileName(outputFiles.get(0)) + (i+1) + ".md", force);
						}
						
					} else if (outputFiles.size()==inputFiles.size()) {
						for (int i=0; i<inputFiles.size(); ++i) {
							giveToConverter(inputFiles.get(i), outputFiles.get(i), force);
						}
						
					} else throw new ArgumentsException();
				}
			}
			
		} catch (ArgumentsException e) {
			System.out.println("Ervenytelen argumentumok!");
			if (e.getMessage()!=null) System.out.println("Hiba: " + e.getMessage());
			System.out.println("segitseg: \"help\" v. \"info\"");
		}
	}
	
	// visszater a Stringben szereplo utolso "." elotti resszel, vagy az egesz String-el, ha nincs "."
	public static String getFileName(String file) {
		if (file.lastIndexOf(".")==-1) return file;
		else if (file.lastIndexOf(".")==0) return null;
		else return file.substring(0, file.lastIndexOf("."));
	}
	
	//visstater a Stringben szereplo utolso "." utani resszel, vagy null-al, ha nincs "."
	public static String getFileFormat(String file) {
		if (file.lastIndexOf(".")==-1) return null;
		else if (file.length()-1 <= file.lastIndexOf(".")) return null;
		else return file.substring(file.lastIndexOf(".")+1);
	}
	
	// "atadja" a konvertalni kivant fajlt, es a celfajt a megfelelo konvertalo osztalynak
	private static void giveToConverter(String inFileName, String outFileName, boolean force) {
		try {
			File outFile = new File(outFileName);
			if (outFile.exists()) {
				Converter plugin;
				if (force) {
					outFile.delete();
					plugin =  PluginManager.getConverter(inFileName);
					if(plugin != null) {
						plugin.convert(inFileName, outFileName);
					}
					else {
						System.out.println("Hiba: " + inFileName + " nem konvertálható!");
						return;
					}
					
					System.out.println("Konvertálás: " + inFileName + " -> " + outFileName);
				} else {
					System.out.println("A celfajl mar letezik: " + outFileName);
				}
			} else {
				Converter plugin;
				plugin =  PluginManager.getConverter(inFileName);
				if(plugin != null) {
					plugin.convert(inFileName, outFileName);
				}
				else {
					System.out.println("Hiba: " + inFileName + " nem konvertálható!");
					return;
				}
				System.out.println("Konvertálás: " + inFileName + " -> " + outFileName);
			}
		} catch (Exception e) {
			System.out.println("Hiba a fajl konvertalasa soran: " + inFileName);
			//e.printStackTrace();
		}
	}
}