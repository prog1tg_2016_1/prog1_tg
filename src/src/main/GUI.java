package main;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUI {

	private JFrame frmSzvegkonvertl;
	private JTextField txt_FileIn;
	private JTextField txt_FileOut;
	private JTextPane text_Output;

	/**
	 * Launch the application.
	 * @wbp.parser.entryPoint
	 */
	public static void main(String[] args) {
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmSzvegkonvertl.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
		
		System.out.println("Ez az MD Konvert�l� program GUI-ja. Sajnos m�g nincs k�sz, �gy a jelenlegi verzi�ban a haszn�lata nem t�mogatott!");
		//loadGUI();
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void loadGUI() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmSzvegkonvertl.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSzvegkonvertl = new JFrame();
		frmSzvegkonvertl.setTitle("Sz\u00F6vegkonvert\u00E1l\u00F3");
		frmSzvegkonvertl.setBounds(100, 100, 702, 405);
		frmSzvegkonvertl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		txt_FileIn = new JTextField();
		txt_FileIn.setEditable(false);
		txt_FileIn.setText("bemeneti f\u00E1jln\u00E9v");
		txt_FileIn.setColumns(10);
		
		txt_FileOut = new JTextField();
		txt_FileOut.setText("kimeneti f\u00E1jln\u00E9v");
		txt_FileOut.setColumns(10);
		
		JButton btn_FileInBrowse = new JButton("Tall\u00F3z\u00E1s...");
		
		JButton btn_FileOutBrowse = new JButton("Tall\u00F3z\u00E1s...");
		
		JButton btn_StartConversion = new JButton("Konvert\u00E1l\u00E1s");
		btn_StartConversion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// ...
			}
		});
		btn_StartConversion.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JTextPane text_OutputLog = new JTextPane();
		text_OutputLog.setEditable(false);
		text_OutputLog.setText("**********");
		text_Output = text_OutputLog;
		
		
		JCheckBox checkbox_convertFolders = new JCheckBox("Mappa konvert\u00E1l\u00E1sa");
		
		JLabel lbl_conversionStatus = new JLabel("K\u00E9szenl\u00E9t");
		GroupLayout groupLayout = new GroupLayout(frmSzvegkonvertl.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(14)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(checkbox_convertFolders)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(text_OutputLog, GroupLayout.DEFAULT_SIZE, 656, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(lbl_conversionStatus)
										.addComponent(txt_FileOut, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
										.addComponent(txt_FileIn, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE))
									.addGap(34)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(btn_StartConversion)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
											.addComponent(btn_FileOutBrowse, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(btn_FileInBrowse, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
							.addGap(16))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addComponent(checkbox_convertFolders)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txt_FileIn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn_FileInBrowse))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txt_FileOut, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btn_FileOutBrowse))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btn_StartConversion)
						.addComponent(lbl_conversionStatus))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(text_OutputLog, GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
					.addContainerGap())
		);
		frmSzvegkonvertl.getContentPane().setLayout(groupLayout);
		
		JMenuBar menuBar = new JMenuBar();
		frmSzvegkonvertl.setJMenuBar(menuBar);
		
		JMenu mnSg = new JMenu("S\u00FAg\u00F3");
		menuBar.add(mnSg);
		
		JMenuItem mntmNvjegy = new JMenuItem("N\u00E9vjegy");
		mnSg.add(mntmNvjegy);
	}
	
	public void writeLn(String text) {
		if(text_Output != null) {
			text_Output.setText(text_Output.getText() + "\n" + text);
		}
	}
}
