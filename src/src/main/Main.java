package main;

import plugins.*;

/** @deprecated
 * @see MainCMD */
public class Main {

	static boolean useGUI = false;
	static GUI guiInstance;
	
	public static void main(String[] args) {
		// TODO Create something useful.
		
		/* idea for plug-in handling #1:
		 * 1. Load list of plug-ins from a plugins.txt file
		 * 2. Load plug-ins using reflection
		 * 3. Wait for user input (folders/files to convert)
		 * 4. Handle conversion
		 * 
		 * Idea #2:
		 * 1. Wait for user input (folders/files to convert)
		 * 2. Try to load necessary plug-ins (through plugins.txt)
		 * 3. Handle conversion
		 * 
		 * GUI:
		 * output will be universal, done through a custom "writeLog" method.
		 */
		
		
		Converter[] plugins;
		PluginManager manager = new PluginManager();
		
		for(int i = 0; i < args.length; i++) {
			switch(args[i]) {
			case "-gui": 
				if(useGUI)
					break;
				
				writeLog("## Launching GUI.");
				useGUI = true;
				guiInstance = new GUI();
				guiInstance.main(null);
				
				writeLog("## GUI launched.");
				
				break;
			}
		}
		
		plugins = manager.getAvailablePlugins();
		for(int i=0; i < plugins.length; i++) {
			writeLog(manager.getAttribute(plugins[i], "format") + " converter written by " + manager.getAttribute(plugins[i], "author"));
		}
		
		
		
	}
	
	public static void writeLog(String text) {
		if(useGUI) {
			guiInstance.writeLn(text);
		}
		else {
			System.out.println(text);
		}
	}

}
