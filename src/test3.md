�

Docx4j - Getting Started

This guide is for docx4j **2.8.0**, and was last updated in May 2012.

The latest version of this document can always be found in [docx4j on GitHub in /docs][docx4j on GitHub in _docs] (in Flat OPC XML format for Word 2007).

The most up to date copy of this document is in English. From time to time, it is machine translated into other languages. 

What is docx4j?

docx4j is a library for unzipping a docx "package", and parsing the WordprocessingML XML to create an in-memory representation in **Java**. Recent versions of docx4j also support Powerpoint pptx files and Excel xlsx files.

It is similar in concept to Microsoft's OpenXML SDK, which is for .NET.

docx4j is open source, available under the Apache License (v2). As an open source project, docx4j has been substantially improved by a number of contributions (see the README or POM file for contributors), and further contributions are always welcome. Please see the docx4j forum at [http://www.docx4java.org/forums/][http_www.docx4java.org_forums] for details.

**The Docx4j social contract**

docx4j is currently available under the Apache Software license. This gives you freedom to do pretty much anything you like with it. It also means you don't have to pay for it (there is no incentive to take up a commercial license, so we don't offer one).

The ***quid pro quo*** is that if docx4j helps you out, you should ***please*** "give something back", by way of code, community support, by "spreading the word" (promotion), or by buying commerical development services. Your choice.docx4j needs you help to make it easier for people to find it. 

If you choose promotion, your options include:

emailing to jharrop@plutext.com a�testimonial which we can put on our website (preferably with your organization name, but without is worthwhile as well), 

a blog�post, a tweet, or a helpful (non-spammy) comment in an online forum,

sharing the content on our blog, following jasonharrop on Twitter, or connecting on LinkedIn.

Your promotion/support will help grow the docx4j community and thus its strength, to the benefit of all. 

Please complete our very short new user survey at [http://www.plutext.com/limesurvey/index.php?sid=78372][http_www.plutext.com_limesurvey_index.php_sid_78372]. It includes a question on the above. Thanks.

Docx4j relies heavily on **JAXB**, the JCP standard for Java - XML binding. You can think of docx4j as a JAXB implementation of (amongst others):

Open Packaging Conventions

WordProcessingML (docx) part of Open XML

Presentation ML (pptx) part of OpenXML 

SpreadsheetML (xlsx) part of Open XML.

The library is designed to round trip docx files with 100% fidelity, and supports all 2007 WordML. Support for new Word 2010 features will be added soon.

The docx4j project is sponsored by Plutext ([www.plutext.com][]).

Is docx4j for you?

Docx4j is for processing docx documents (and pptx presentations and xlsx spreadsheets) in Java.

It isn't for old binary (.doc) files. If you wish to invest your effort around docx (as is wise), but you also need to be able to handle old doc files, see further below for your options. 

Nor is it for RTF files.

If you want to process docx documents on the .NET platform, you should look at Microsoft's OpenXML SDK instead.

An alternative to docx4j is Apache POI. I'd particularly recommend that if you are only processing Excel documents, and need support for the old binary xls format. Since POI uses XmlBeans (not JAXB) it may be a better choice if you want to use XmlBeans.

What sorts of things can you do with docx4j?

Open existing docx (from filesystem, SMB/CIFS, WebDAV using VFS), pptx, xlsx

Create new docx, pptx, xlsx

Programmatically manipulate the above (of course)

Do all this on Android (v3 or 4).

Specific to docx4j (as opposed to pptx4j, xlsx4j):

Import XHTML

Template substitution; CustomXML binding

Produce/consume Word 2007's xmlPackage (pkg) format

Save docx to filesystem as a docx (ie zipped), or to JCR (unzipped)

Apply transforms, including common filters

Export as HTML or PDF

Diff/compare documents, paragraphs or sdt (content controls)

Font support (font substitution, and use of any fonts embedded in the document)

This document focuses primarily on docx4j, but the general principles are equally applicable to pptx4j and xlsx4j.

***docx4all***  is an example of an application based on docx4j; its a Swing-based wordprocessor for docx documents. You can try it or download its source code at **www.docx4java.org**

�

What Word documents does it support?

Docx4j can read/write docx documents created by or for Word 2007, or earlier versions which have the compatibility pack installed.

The relevant parts of docx4j are generated from the ECMA schemas.

It can't read/write Word 2003 XML documents. The main problem with those is that the XML namespace is different.

Docx4j 2.7.1 handles Word 2010 specific features, by gracefully degrading to the specified 2007 

For more information, please see ***Specification versions*** below.

Handling legacy binary .doc files

Apache POI's HWPF can read .doc files, and docx4j could use this for basic conversion of .doc to .docx. The problem with this approach is that POI's HWPF code fails on many .doc files.

An effective approach is to use OpenOffice (via jodconverter) to convert the doc to docx, which docx4j can then process. If you need to return a binary .doc, OpenOffice/jodconverter can convert the docx back to .doc.

There is also http://b2xtranslator.sourceforge.net/ . If a pure Java approach were required, this could be converted.

Getting Help: the docx4j forum

Free community support is available in the docx4j forum, at [http://www.docx4java.org/forums/][http_www.docx4java.org_forums]and on Stack Overflow.

Before posting, please:

check this document doesn�t answer your question

tryto help yourself: people are unlikely to help you if it looks like you are asking someone else to do lots of work you presumably are being paid to do!

ensureyour post says which version of docx4j you are using, and contains your Java code (between \[java\] .. and .. \[/java\]) andXML (between \[xml\] .. and .. \[/xml\]), and if appropriate a docx/pptx/xlsx attachment

consider browsing relevant docx4j source code 

This discussion is generally in English. If you can volunteer to moderate a forum in another language (for example, French, Chinese, Spanish�), please let us know.

�

Using docx4j binaries

You can download the latest version of docx4j from [http://www.docx4java.org/docx4j/][http_www.docx4java.org_docx4j]

In general, we suggest you develop against a currently nightly build, since the latest formal release can often be several months old.

Supporting jars can be found in the .tar.gz version, or in the relevant subdirectory. 

Command Line Samples

With docx4j version 2.6.0, there are several samples you can run right away from the command line.

The two to try (both discussed in detail further below) are:

OpenMainDocumentAndTraverse

PartsList

Invoke with a command like:

���java \-cp docx4j.jar:log4j-1.2.15.jar org.docx4j.samples.OpenMainDocumentAndTraverse \[input.docx\]

If there are any images in the docx, you'd also need:  
  
���xmlgraphics-commons-1.4.jar  
  
���commons-logging-1.1.1.jar

on your classpath.

docx4j dependencies

log4j

To do anything with docx4j, you need **log4j** on your classpath.

To actually enable logging, log4j usually requires a log4.properties or log4j.xml on your class path. See for example [http://www.docx4java.org/trac/docx4j/browser/trunk/docx4j/src/main/resources/log4j.xml][http_www.docx4java.org_trac_docx4j_browser_trunk_docx4j_src_main_resources_log4j.xml]If you don't configure log4j like that, docx4j will auto configure logging at INFO level. You can disable the autoconfiguration by setting docx4j property "docx4j.Log4j.Configurator.disabled" to true.

If you are using Eclipse to run things, in the run configuration:

add VM argument   
  
���\-Dlog4j.configuration=log4j.xml  


to the classpath, add a user entry (click "advanced..") for  
  
���src/main/resources

images

If there are any images in the docx, you'll also need:  
  
���xmlgraphics-commons-1.4.jar  
  
���which in turn requires commons-logging-1.1.1.jar

other dependencies 

Depending what you want to do, the other dependencies will be required. The following table explains the other dependencies:

<table style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;position: relative; margin-left: 0in;table-layout: fixed;vertical-align: top;border-collapse: collapse;width: 6.65in;">
 <tbody>
  <tr>
   <td style="background-color: #DDD9C3;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">Functionality</span></p></td>
   <td style="background-color: #DDD9C3;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">Jar</span></p></td>
   <td style="background-color: #DDD9C3;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">which also requires</span></p></td>
  </tr>
  <tr>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">HTML export</span></p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">Xalan</span></p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
  </tr>
  <tr>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">PDF export</span></p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">Xalan,</span><br /><span style="font-family: 'Calibri';">FOP</span></p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p><span style="font-family: 'Calibri';">commons-io</span></p> <p><span style="font-family: 'Calibri';">avalon-framework api &amp; impl</span></p></td>
  </tr>
  <tr>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
  </tr>
  <tr>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
  </tr>
  <tr>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
  </tr>
  <tr>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
   <td style="border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: #000000;border-left-style: solid;border-left-width: 1px;border-left-color: #000000;border-right-style: solid;border-right-width: 1px;border-right-color: #000000;border-top-style: solid;border-top-width: 1px;border-top-color: #000000;padding-bottom: 0mm;padding-left: 1.91mm;padding-right: 1.91mm;padding-top: 0mm;"> <p>&nbsp;</p></td>
  </tr>
 </tbody>
</table>

�

�

1

�


[docx4j on GitHub in _docs]: https://github.com/plutext/docx4j/tree/master/docs
[http_www.docx4java.org_forums]: http://www.docx4java.org/forums/
[http_www.plutext.com_limesurvey_index.php_sid_78372]: http://www.plutext.com/limesurvey/index.php?sid=78372&lang=en
[www.plutext.com]: http://www.plutext.com
[http_www.docx4java.org_docx4j]: http://www.docx4java.org/docx4j/
[http_www.docx4java.org_trac_docx4j_browser_trunk_docx4j_src_main_resources_log4j.xml]: http://dev.plutext.org/trac/docx4j/browser/trunk/docx4j/src/main/resources/log4j.xml