## Programozás I tantárgy / 2015-2016-2 / Tehetséggondozós projektmunka ##

### Tagok ###
- Gera Imre (repó tulajdonos és adminisztrátor)
- Kocsár Máté
- Kovács Marcell
- Bencsik Dávid

### Fontos információk ###
Használt IDE: Eclipse + git.

*[Git telepítési és konfigurációs útmutató](https://drive.google.com/file/d/0B9VemlIpoxl_bTJJOU5nN3JmTDg/view?usp=sharing)*

*[Komponensek (és fejlesztőik)](https://docs.google.com/spreadsheets/d/1qGWQZw6OykA0v9soNe7LGziy3iY3AAQptFfBoBpmksw/edit?usp=sharing)*

*[Specifikáció](https://onedrive.live.com/redir?page=view&resid=58E945DC6D9EE583!124855&authkey=!AP9gHFN37RGfJRY)*

### Főágak (irányelvek) ###
- **master**: kizárólag forduló, működő kódot tartalmazzon! Itt lesz a végleges kód.
- **további ágak**: talán az lenne a legjobb, ha minden egyes komponensnek (pl. PDF -> MD átalakító) saját ága lenne. Ha a komponens fejlesztői befejezték a fejlesztést, egy *pull request*tel jelzik, hogy részükről mehet a merge a főágba (*master*). Ez azt jelenti, hogy az ott lévő kódot a többiek is letölthetik és kivizsgálhatják, megjegyzéseket fűzhetnek hozzá és további javításokat végezhetnek rajta, majd, ha mindenki elfogadta, elvégezzük a merge-öt. 
A pull request működéséről további információ [itt található](https://www.atlassian.com/git/tutorials/making-a-pull-request/how-it-works). *További megjegyzés: a dokumentáció szerkesztése előreláthatólag OneDrive-on (Office 365) keresztül történik majd, de ha ki lesznek osztva egy-egy emberre, akkor azok is mehetnek a repóra ágakként.*

### Komponensek ###
- Főprogram (modul-összekötő)
- Konvertáló egységek/modulok (TXT/HTML/DOC(X)/ODT/PDF -> MD) (min. 4)
- *GUI*
- Specifikáció
- Rendszerterv (UML)
- Teszt jegyzőkönyv
- Prezentáció

### Projekt kiírás jegyzete ###
Fájlkonverzió: .txt, .html, .odt, .doc(x), .pdf -> .md
Parancssor elég, de GUI sem hátrány.
Külső library letölthető és használható (pl. fájlformátum kezelők).

FONTOS: ez csapatmunka, teljes szoftverfejlesztési folyamat!!

**1. SPECIFIKÁCIÓ**
	Dokumentum, ami hétköznapi nyelven (nem túlzottan informatikai, mint egy felhasználói kézikönyv) íródott.
	1-2 oldal
	(ezt kell lefejleszteni)

**2. RENDSZERTERV**
	UML ábrahalmaz, osztálydiagramok.

**3. IMPLEMENTÁCIÓ/PROGRAMKÓD**
	*' Legyen világosan elkülöníthető, hogy ki mit csinált!*
	Ahány tagú a csapat, minimum annyi bemeneti formátumot kell tudni kezelni!

	Extrák:
	- GUI
	- Plug-In kezelés (reflection mechanizmus!)

**4. TESZT JEGYZŐKÖNYV**
	Teszt esetek: bemenetre (használati módra) milyen kimenetet (eredményt) ad a program
	(végfelhasználói szemmel meghatározott programfuttatási lépéssorozat)
	min. annyi, ahány fájlformátum van
	
**5. PREZENTÁCIÓ (ÁTADÁS/ÁTVÉTEL)**
	Valamilyen prezentációs szoftverrel bemutatni a programot, akár a feladatot is
	
Határidő: utolsó gyakorlat (2016. 05. 11.)
Bemutatás: 20 percben a kivetítő segítségével, lehet hozni saját laptopot erre a célra.